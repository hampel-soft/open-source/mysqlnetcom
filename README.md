# MySQLNetCom

An open-source implementation of the MySQL Network Communication Protocol using pure TCP/IP primitives for talking to a MySQL server via Ethernet. The VIs in this repository are provided and maintained by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com).

## :open_file_folder: Origin

The original source code was developed by [Marc Christenson, Sisu Devices](https://forums.ni.com/t5/user/viewprofilepage/user-id/36077), and shared on [NI's Example Program Code Exchange forum](https://forums.ni.com/t5/Example-Code/Native-LabVIEW-TCP-IP-Connector-for-mySQL-Database/ta-p/3496603).

## :rotating_light: Not For Production Use!

This is a reference implementation that has NOT been vetted for a production setting. It might or might not meet traditional requirements in terms of uptime, security, absence of bugs, etc. Use at your own risk!

## :wrench: LabVIEW 2016

The VIs are maintained in LabVIEW 2016 (16.0f5 (32-bit) to be precise).

## :bulb: Documentation

The HSE Dokuwiki holds more information on the protocol and these VIs:

* https://dokuwiki.hampel-soft.com/code/open-source/mysqlnetcom

## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on [how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). Please get in touch at (office@hampel-soft.com) for any questions.

##  :beers: Credits

* Marc Christenson
* Steve Watts
* Mark Yedinak
* Joerg Hampel
* Manuel Sebald

## :page_facing_up: License 

This library is licensed under the BSD 4-clause License - see the [LICENSE](LICENSE) file for details
